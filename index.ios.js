/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TabBarIOS,
} from 'react-native';

export default class ReactNativeAirbnb extends Component {
  constructor(props) {
     super(props);
     this.state = {
      selectedTab: 'EXPLORE'
     };
  }
  _renderContent(color: string, pageText: string) {
    return (
      <View style={[styles.tabContent, {backgroundColor: color}]}>
      <Text style={styles.tabText}>{pageText}</Text>
      </View>
      );
  }
  render() {
    return (
      <View style={{flex:1}}>
      <TabBarIOS
      style={{flex:1,alignItems:"flex-end"}}
      tintColor='#fc5c50'
      barTintColor='white'>
      <TabBarIOS.Item
      title='EXPLORE'
      icon={require('./assets/explore.png')}
      selected={this.state.selectedTab === 'EXPLORE'}
      onPress={() => {
        this.setState({
          selectedTab: 'EXPLORE',
        });
      }}
      >
      {this._renderContent('#414A8C', 'EXPLORE')}
      </TabBarIOS.Item>
      <TabBarIOS.Item
      title='PROFILE'
      icon={require('./assets/user.png')}
      selected={this.state.selectedTab === 'PROFILE'}
      onPress={() => {
        this.setState({
          selectedTab: 'PROFILE',
        });
      }}
      >
      {this._renderContent('#783E33', 'PROFILE')}
      </TabBarIOS.Item>
      </TabBarIOS>
      </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  tabContent: {
    flex: 1,
    alignItems: 'center',
  },
  tabText: {
    color: 'white',
    margin: 50,
  },
});

AppRegistry.registerComponent('ReactNativeAirbnb', () => ReactNativeAirbnb);
